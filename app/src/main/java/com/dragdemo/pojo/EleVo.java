package com.dragdemo.pojo;

import com.dragdemo.DefaultData;

import java.util.ArrayList;
import java.util.List;

public class EleVo {
    private String id;
    private String text;
    private int type;//1：设备  2：房间
    private List<EleVo> eleVos = new ArrayList<EleVo>();

    public EleVo(String text, int type, List<EleVo> eleVos) {
        id = DefaultData.getInstance().getUUID();
        this.text = text;
        this.type = type;
        this.eleVos = eleVos;
    }

    public int getType() {

        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<EleVo> getEleVos() {
        return eleVos;
    }

    public void setEleVos(List<EleVo> eleVos) {
        this.eleVos = eleVos;
    }

    public String getId() {
        return id;
    }

}
