package com.dragdemo;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;

import com.dragdemo.adapter.DragElecQuickAdapter;
import com.dragdemo.pojo.EleVo;
import com.dragdemo.ui.RoomActivity;
import com.dragdemo.view.DragGridView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private final String TAG = "MainActivity";

    private DragGridView grid_drag;
    private DragElecQuickAdapter elecQuickAdapter;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;
        grid_drag = findViewById(R.id.grid_drag);
        initData();
    }

    private void initData(){
        elecQuickAdapter = new DragElecQuickAdapter(context,R.layout.drag_item,DefaultData.itemList);
        grid_drag.setAdapter(elecQuickAdapter);
        elecQuickAdapter.refresh(DefaultData.itemList);

        grid_drag.setSelector(new ColorDrawable(Color.TRANSPARENT));
        grid_drag.setOnItemClickListener(new ItemClickListener());
        grid_drag.setOnItemDragListener(new DragGridView.OnItemDragListener() {
            @Override
            public void setOnDragEnd(Object from, Object to) {
                if(null != from && from instanceof EleVo
                        && null != to && to instanceof  EleVo){
                    addElectricalToRoom((EleVo)from,(EleVo)to);
                }
            }

            @Override
            public void setOnStartDrag() {
                Log.e(TAG,"开始setOnStartDrag");
            }

            @Override
            public void setOnEndDrag() {
                Log.e(TAG,"结束setOnEndDrag");

            }

            @Override
            public void setOnDeletePos(int pos) {
                Log.e(TAG,"setOnDeletePos pos=="+pos);
            }
        });
    }

    private void addElectricalToRoom(EleVo from,EleVo to){
        //判断房间是否存在
        if(null == from || null == to){
            Log.e(TAG,"拖到对象为空");
            return;
        }
        if(from.getType() !=1 ){
            Log.e(TAG,"非设备不能拖");
            return;
        }
        if(from.getId().equals(to.getId())){
            Log.e(TAG,"相同的设备，无效操作");
            return;
        }

        if(to.getType() == 2){
            to.getEleVos().add(from);
            DefaultData.itemList.remove(from);
        }else{
            List<EleVo> eleVoList = new ArrayList<>();
            eleVoList.add(from);
            eleVoList.add(to);
            EleVo vo = new EleVo("房间",2,eleVoList);
            DefaultData.itemList.remove(from);
            DefaultData.itemList.remove(to);
            DefaultData.itemList.add(vo);
        }

        elecQuickAdapter.refresh(DefaultData.itemList);
    }


    @Override
    protected void onResume() {
        super.onResume();
        elecQuickAdapter.refresh(DefaultData.itemList);
    }

    private class ItemClickListener implements AdapterView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> arg0, View view, int pos,
                                long arg3) {
            if(null != arg0.getItemAtPosition(pos) && arg0.getItemAtPosition(pos) instanceof EleVo){
                EleVo eleVo = (EleVo) arg0.getItemAtPosition(pos);
                if(eleVo.getType() == 2){
                    Intent intent = new Intent(context, RoomActivity.class);
                    intent.putExtra("uuid",eleVo.getId());
                    context.startActivity(intent);
                }
            }
        }
    }
}
