package com.dragdemo.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.dragdemo.R;
import com.dragdemo.pojo.EleVo;
import com.dragdemo.view.DragGridListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public  class DragRoomElecQuickAdapter extends ArrayAdapter<EleVo> implements DragGridListener {
    private List<EleVo> items;
    private Context context;
    private int resource;


    public DragRoomElecQuickAdapter(Context context, int resource, List<EleVo> items) {
        super(context, resource);
        this.context = context;
        this.resource = resource;
        this.items = items;
    }


    public void refresh(List<EleVo> objects){
        List<EleVo> objectsNew = new ArrayList<>();
        objectsNew.addAll(objects);
        items.clear();
        items.addAll(objectsNew);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public EleVo getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View itemView = convertView;
        EleVo item = items.get(position);
        ViewHolder viewHolder = null;
        if (itemView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            viewHolder = new ViewHolder();
            itemView = inflater.inflate(resource, null);
            viewHolder.text = (TextView) itemView.findViewById(R.id.text);
            itemView.setTag(viewHolder);
        }else{
            viewHolder =  (ViewHolder) itemView.getTag();
        }

        viewHolder.text.setText(item.getText());
        return itemView;
    }

    @Override
    public void reorderItems(int oldPosition, int newPosition) {
        EleVo temp = getItem(oldPosition);
        if(oldPosition < newPosition){
            for(int i=oldPosition; i<newPosition; i++){
                Collections.swap(items, i, i+1);
            }
        }else if(oldPosition > newPosition){
            for(int i=oldPosition; i>newPosition; i--){
                Collections.swap(items, i, i-1);
            }
        }
        items.set(newPosition, temp);
    }

    @Override
    public void setHideItem(int hidePosition) {
        notifyDataSetChanged();
    }

    @Override
    public void removeItem(int deletePosition) {
        items.remove(deletePosition);
        notifyDataSetChanged();
    }

    @Override
    public Object getHoldOnItem(int pos){
        try {
            if(pos==-9){
                return null;
            }
            if(items.size()<pos){
                return null;
            }
            return items.get(pos);
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    class ViewHolder {
        TextView text;
    }
}
