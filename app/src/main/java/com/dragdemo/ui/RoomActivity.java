package com.dragdemo.ui;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dragdemo.DefaultData;
import com.dragdemo.R;
import com.dragdemo.adapter.DragElecQuickAdapter;
import com.dragdemo.adapter.DragRoomElecQuickAdapter;
import com.dragdemo.pojo.EleVo;
import com.dragdemo.view.DragGridView;

import java.util.ArrayList;
import java.util.List;

public class RoomActivity extends Activity {
    private final String TAG = "MainActivity";

    private DragGridView grid_drag;
    private EditText text_name;
    private LinearLayout layout_bj;
    private DragRoomElecQuickAdapter elecQuickAdapter;
    private Context context;
    private String uuid;
    private EleVo eleVo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drag_room);
        context = this;
        uuid = getIntent().getStringExtra("uuid");
        grid_drag = findViewById(R.id.grid_drag);
        text_name = findViewById(R.id.text_name);
        layout_bj = findViewById(R.id.layout_bj);

        initData();
    }

    private void initData(){
        if(uuid == null){
            finish();
            return;
        }
        for(EleVo vo : DefaultData.itemList){
            if(uuid.equals(vo.getId())){
                eleVo = vo;
                break;
            }
        }

        if(eleVo == null){
            finish();
            return;
        }
        text_name.setText(eleVo.getText());
        elecQuickAdapter = new DragRoomElecQuickAdapter(context,R.layout.drag_room_item,eleVo.getEleVos());
        grid_drag.setAdapter(elecQuickAdapter);
        elecQuickAdapter.refresh(eleVo.getEleVos());

        grid_drag.setSelector(new ColorDrawable(Color.TRANSPARENT));
        grid_drag.setOnItemClickListener(new ItemClickListener());
        grid_drag.setHead_height(dip2px(context,110));
        grid_drag.setGridview_height(dip2px(context,300));
        grid_drag.setOnItemDragListener(new DragGridView.OnItemDragListener() {
            @Override
            public void setOnDragEnd(Object from, Object to) {
                if(null != from && from instanceof EleVo
                        && null != to && to instanceof  EleVo){

                }
            }

            @Override
            public void setOnStartDrag() {
                Log.e(TAG,"开始setOnStartDrag");
            }

            @Override
            public void setOnEndDrag() {
                Log.e(TAG,"结束setOnEndDrag");

            }

            @Override
            public void setOnDeletePos(int pos) {
                Log.e(TAG,"setOnDeletePos pos=="+pos);
                if(pos != -1 && pos<eleVo.getEleVos().size()){
                    EleVo vo = eleVo.getEleVos().get(pos);
                    remoteElectricalToRoom(vo);
                }
            }
        });
    }

    private void remoteElectricalToRoom(EleVo vo){
        //判断房间是否存在
        if(null == vo){
            Log.e(TAG,"拖到对象为空");
            return;
        }
        eleVo.getEleVos().remove(vo);
        DefaultData.itemList.add(vo);
        if(eleVo.getEleVos().size() == 0){
            DefaultData.itemList.remove(eleVo);
            finish();
            return;
        }
        elecQuickAdapter.refresh(eleVo.getEleVos());
    }


    @Override
    public void finish() {
        super.finish();
        eleVo.setText(text_name.getText().toString());
    }

    private class ItemClickListener implements AdapterView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> arg0, View view, int pos,
                                long arg3) {
            Object obj = arg0.getItemAtPosition(pos);
            Log.e(TAG,this.getClass().getSimpleName()+"ItemClickListener  obj="+obj);
        }
    }

    public int dip2px(final Context context, final float n) {
        return (int)(0.5f + n * context.getResources().getDisplayMetrics().density);
    }
}
