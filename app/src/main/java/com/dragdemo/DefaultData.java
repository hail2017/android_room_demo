package com.dragdemo;

import com.dragdemo.pojo.EleVo;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class DefaultData {
    private static DefaultData instance = null;
    public static  DefaultData getInstance(){
        if(null == instance){
            synchronized (DefaultData.class) {
                if(null == instance){
                    instance = new DefaultData();
                }
            }

        }
        return instance;
    }

    public static List<EleVo> itemList = new ArrayList<>();

    static {
        itemList.add(new EleVo("开关",1,null));
        itemList.add(new EleVo("插座",1,null));
        itemList.add(new EleVo("门锁",1,null));
        itemList.add(new EleVo("门磁",1,null));
        itemList.add(new EleVo("烟感",1,null));
        itemList.add(new EleVo("漏水",1,null));
        itemList.add(new EleVo("燃气",1,null));
        itemList.add(new EleVo("移动",1,null));

    }

    public String getUUID() {
        String s = UUID.randomUUID().toString();
        // 去掉“-”符号
        return s.substring(0, 8) + s.substring(9, 13) + s.substring(14, 18)
                + s.substring(19, 23) + s.substring(24);
    }
}
